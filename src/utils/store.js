/**
 * Created by Luby on 2018/11/21.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import { stat } from 'fs';

Vue.use(Vuex);
let store = new Vuex.Store({
  // 1. state
  state:{
    common:{
      allowBack: true // 控制是否允许后退
    },
    staffid:"",//员工id
    activityDetails:{},
    activityList:{},
    activitySignup:{},
    binding:{},
    editInformation:{},
    editName:{},
    editPhone:{},
    hobby:{},
    home:{},
    IntegralBill:{},
    integralChange:{},
    IntegralEarn:{},
    login:{
      hobby:"", // 兴趣爱好
      family:"", // 家庭结构
      hasChildNumber:"", //小孩数量
      childsAge:"", // 小孩年龄段
      isKnowMember:"",// 知道其他新中会成员
      memberPhone:"", // 新中会电话号码
      relationship:"", //会员关系
      isTenant:"", // 是否写字楼租户
      work :"", // 职业
      haveCar : "", // 是否有车
      workArea:"", // 工作区域
      workAddr:"", // 工作单位
      OfficeBuilding : "",// 办公楼宇
    },
    login6:{},
    login7:{},
    login8:{},
    login9:{},
    memberEquity:{},
    mineInformation:{},
    myPoints:{},
    perfectInformation:{},
    personalInformation:{},
    validateNewPhone:{},
    projectobj:{},//项目名称和项目id,
    cordtype:"",//币种
    expensedetailobj:{},//菜单页的路径参数
    key:'',//标示页面是否从复制进来
    menutype:"交通费",
    tnum:0,//交通序号
    fnum:0,//飞机序号
    hnum:0,//火车序号
    znum:0,//住宿序号
    dnum:0,//招待序号
    jnum:0,//团建序号
    bnum:0,//加班序号
    qnum:0,//其它序号
    sslist:[],//实施项目列表
    whlist:[],//维护项目列表
    gblist:[],//关闭项目列表
    isplist:'',
    scrolltop:''
  },

  // // 2. getters
  getters:{
    // 参数列表state指的是state数据
    // 获取是否可跳转的参数
    getAllowBack(state){
      return state.common.allowBack;
    }
  },
  // 3. actions
  // 通常跟api接口打交道
  actions:{

    // 参数列表：{commit, state}
    // state指的是state数据
    // commit调用mutations的方法
    // name就是调用此方法时要传的参数
    setCityName({commit,state}, array){
      console.log("setCityName",array);
      commit("setData", array);
    },
    updateAppSetting({commit,state}, array){
      commit("setData", array);
    }
  },
  // 4. mutations
  mutations:{
    // state指的是state的数据
    // name传递过来的数据 数组接口，1为页面名，2为页面下的对象名，3为赋值参数
    updateAppSetting(state, result){
      console.log("updateAppSetting",result);
      state.common.allowBack = result;
    },
    setData(state, array){
      console.log('setDate',array);
      state[array[0]][array[1]] = array[2];
    },
    getstaffid(state,getvalue){
      state.staffid=getvalue;
    },
    getprojectobj(state,getvalue){
      state.projectobj=getvalue;
    },
    getcord(state,getvalue){
      state.cordtype=getvalue;
    },
    getexpensedetail(state,getvalue){
      state.expensedetailobj=getvalue;
    },
    getkey(state,getvalue){
      state.key=getvalue;
    },
    getmenutype(state,getvalue){
      state.menutype=getvalue;
    },
    gettnum(state,getvalue){
      state.tnum++;
    },
    getfnum(state,getvalue){
      state.fnum++;
    },
    gethnum(state,getvalue){
      state.hnum++;
    },
    getznum(state,getvalue){
      state.znum++;
    },
    getdnum(state,getvalue){
      state.dnum++;
    },
    getjnum(state,getvalue){
      state.jnum++;
    },
    getbnum(state,getvalue){
      state.bnum++;
    },
    getqnum(state,getvalue){
      state.qnum++;
    },
    getsslist(state,getvalue){
      state.sslist=getvalue;
    },
    getwhlist(state,getvalue){
      state.whlist=getvalue;
    },
    getgblist(state,getvalue){
      state.gblist=getvalue;
    },
    getisplist(state,getvalue){
      state.isplist=getvalue;
    },
    getHomeScrollTop(state,getvalue){
      state.scrolltop=getvalue;
    }
  }
});

export default store;
