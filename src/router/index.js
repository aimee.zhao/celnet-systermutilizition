import Vue from 'vue'
import Router from 'vue-router'
// const home = r => require.ensure([], () => r(require('../page/home/home.vue')), 'home');
// const newexpense= r => require.ensure([], () => r(require('../page/newexpense/newexpense.vue')), 'newexpenser');
// const expensedetail= r => require.ensure([], () => r(require('../page/expensedetail/expensedetail.vue')), 'expensedetail');
// const newtraffic= r => require.ensure([], () => r(require('../page/newtraffic/newtraffic.vue')), 'newtraffic');
// const newaccomadation= r => require.ensure([], () => r(require('../page/newaccomadation/newaccomadation.vue')), 'newaccomadation');
// const examineaccount= r => require.ensure([], () => r(require('../page/examineaccount/examineaccount.vue')), 'examineaccount');
// const examinetraffic= r => require.ensure([], () => r(require('../page/examinetraffic/examinetraffic.vue')), 'examinetraffic');
// const examineaccomadation= r => require.ensure([], () => r(require('../page/examineaccomadation/examineaccomadation.vue')), 'examineaccomadation');
// const newsubsidy= r => require.ensure([], () => r(require('../page/newsubsidy/newsubsidy.vue')), 'newsubsidy');
// const subsidydetail= r => require.ensure([], () => r(require('../page/subsidydetail/subsidydetail.vue')), 'subsidydetail');
 const project=r => require.ensure([], () => r(require('../page/project/project.vue')), 'project');
// const searchproject=r=>require.ensure([],()=>r(require('../page/searchproject/searchproject.vue')),'searchproject');
// const copytraffic=r=>require.ensure([],()=>r(require('../page/copytraffic/copytraffic.vue')),'copytraffic');
const index=r=>require.ensure([],()=>r(require('../page/index/index.vue')),'index');
const defendproject=r => require.ensure([], () => r(require('../page/defendproject/defendproject.vue')), 'defendproject');
const searchproject=r => require.ensure([], () => r(require('../page/searchproject/searchproject.vue')), 'searchproject');
const contract=r=>require.ensure([],()=>r(require('../page/contract/contract.vue')),'contract');
const collection=r=>require.ensure([],()=>r(require('../page/collection/collection.vue')),'collection');
const projectday=r=>require.ensure([],()=>r(require('../page/projectday/projectday.vue')),'projectday');
const expense=r=>require.ensure([],()=>r(require('../page/expense/expense.vue')),'expense');
const member=r=>require.ensure([],()=>r(require('../page/member/member.vue')),'member');
const searchmember=r=>require.ensure([],()=>r(require('../page/searchmember/searchmember.vue')),'searchmember');
const projectphase=r=>require.ensure([],()=>r(require('../page/projectphase/projectphase.vue')),'projectphase');
const downfile=r=>require.ensure([],()=>r(require('../page/downfile/downfile.vue')),'downfile');
const pepledetail=r=>require.ensure([],()=>r(require('../page/pepledetail/pepledetail.vue')),'pepledetail');
const account=r=>require.ensure([],()=>r(require('../page/account/account.vue')),'account');
const accountdetail=r=>require.ensure([],()=>r(require('../page/accountdetail/accountdetail.vue')),'accountdetail');
const newaccount=r=>require.ensure([],()=>r(require('../page/newaccount/newaccount.vue')),'newaccount');
const test=r=>require.ensure([],()=>r(require('../page/test/test.vue')),'test');
const personnalday=r=>require.ensure([],()=>r(require('../page/personnalday/personnalday.vue')),'personnalday');
Vue.use(Router);
// survey
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/personnalday'  // 任何没有具体路径的访问，我都让它重定向到home主页，重定向在限制用户手动修改URL时误操作很管用
    },
    {
      path: '/index',
      name: 'index',
      meta: {
        title: '实施项目',
        allowBack: true,
        keepAlive:true,
      },
      component: index,
      
    },
    {
      path: '/project',
      name: 'project',
      meta: {
        title: '实施项目',
        allowBack: true,
        keepAlive:true,
      },
      component: project,
      
    },
    {
      path: '/defendproject',
      name: 'defendproject',
      meta: {
        title: '维护项目',
        allowBack: true,
        keepAlive:true,
      },
      component: defendproject,
    },
    {
      path: '/searchproject',
      name: 'searchproject',
      meta: {
        title: '搜索项目',
        allowBack: true,
        keepAlive:false,
      },
      component: searchproject,
    },
    {
      path: '/contract',
      name: 'contract',
      meta: {
        title: '合同',
        allowBack: true,
        keepAlive:false,
      },
      component: contract,
    },
    {
      path: '/collection',
      name: 'collection',
      meta: {
        title: '收款',
        allowBack: true,
        keepAlive:false,
      },
      component: collection,
    },
    {
      path: '/projectday',
      name: 'projectday',
      meta: {
        title: '项目人天',
        allowBack: true,
        keepAlive:false,
      },
      component: projectday,
    },
    {
      path: '/expense',
      name: 'expense',
      meta: {
        title: '项目费用',
        allowBack: true,
        keepAlive:false,
      },
      component: expense,
    },
    {
      path: '/member',
      name: 'member',
      meta: {
        title: '项目成员',
        allowBack: true,
        keepAlive:false,
      },
      component: member,
    },
    {
      path: '/searchmember',
      name: 'searchmember',
      meta: {
        title: '搜索成员',
        allowBack: true,
        keepAlive:false,
      },
      component: searchmember,
    },
    {
      path: '/projectphase',
      name: 'projectphase',
      meta: {
        title: '项目阶段',
        allowBack: true,
        keepAlive:false,
      },
      component: projectphase,
    },
    {
      path: '/downfile',
      name: 'downfile',
      meta: {
        title: '文件下载',
        allowBack: true,
        keepAlive:false,
      },
      component: downfile,
    },
    {
      path: '/pepledetail',
      name: 'pepledetail',
      meta: {
        title: '消费明细',
        allowBack: true,
        keepAlive:false,
      },
      component: pepledetail,
    },
    {
      path:'/account',
      name:'account',
      meta:{
        title:'客户',
        allowBack:true,
      },
      component:account
    },
    {
      path:'/accountdetail',
      name:'accountdetail',
      meta:{
        title:'客户详情',
        allowBack:true,
      },
      component:accountdetail
    },
    {
      path:'/newaccount',
      name:'newaccount',
      meta:{
        title:'新建客户',
        allowBack:true,
      },
      component:newaccount
    },
    {
      path:'/test',
      name:'test',
      meta:{
        title:'测试',
        allowBack:true,
      },
      component:test
    },
    {
      path:'/personnalday',
      name:'personnalday',
      meta:{
        title:'个人人天使用率',
        allowBack:true,
      },
      component:personnalday
    }
    
    
    // {
    //   path: '/copytraffic',
    //   name: 'copytraffic',
    //   meta: {
    //     title: '查找项目',
    //     allowBack: true
    //   },
    //   component: copytraffic
    // }
  ]
})
